package com.Task_54_40;

import java.util.ArrayList;

class CCar extends CPerson{
	private String modelName;
	private String vid;
	private int yearManufactured;
	private String brand;
	
	/**
	 * List of cars in the system
	 */
	
	ArrayList <CCar> cars = new ArrayList();


	//
	public CCar() {
	}

	//
	public CCar(String paramBrand, int paramYearManufactured, String paramModelName, String paramVid) {
		modelName = paramModelName;
		vid = paramVid;
		this.yearManufactured = paramYearManufactured;
		brand = paramBrand;
	}

	/**
	 * @return the modelName
	 */
	public String getModelName() {
		return modelName;
	}

	/**
	 * @param modelName the modelName to set
	 */
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	/**
	 * @return the vid
	 */
	public String getVid() {
		return vid;
	}

	/**
	 * @param vid the vid to set
	 */
	public void setVid(String vid) {
		this.vid = vid;
	}

	// max speed of vehicle
	public int maxSpeedPerKm() {
		return 200;
	}

	public void print() {
		// TODO Auto-generated method stub
		System.out.println("-----------");
		System.out.println("Brand name is : " +  this.brand);
		System.out.println("yearManufactured is : " +  this.yearManufactured);
		System.out.println("Modal name is : " +  this.modelName);
		System.out.println("Vid is : " +  this.vid);
	};
	
	
}
