package com.Task_54_40;

public class MainPerson {
	public static void main(String[] args) {
		CCar myCar = new CCar("Volvo",2021,"SC40","Vol101");
		CCar myCar2 = new CCar("BMW",2021,"i250","bmw101");
		CPerson myPerson = new CPerson(1,32,"Austin","Tran");
		myPerson.country = "Viet Nam";
		myPerson.addCar(myCar);
		myPerson.addCar(myCar2);
		myPerson.printPerson();
		
	}
}
