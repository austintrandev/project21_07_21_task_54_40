package com.Task_54_40;

import java.util.ArrayList;

public class CPerson {
	private int id;
	private int age;
	private String firstName;
	private String lastName;
	private ArrayList<CCar> cars = new ArrayList();
	public String country;

	public CPerson() {
		
	}
	/**
	 * @param id
	 * @param age
	 * @param firstName
	 * @param lastName
	 */
	public CPerson(int id, int age, String firstName, String lastName) {
		this.id = id;
		this.age = age;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}
	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public ArrayList<CCar> getCars() {
		return cars;
	}

	public void addCar(CCar paramCar) {
		this.cars.add(paramCar);
	}

	// method getfullname of person
	public void getFullName() {
		System.out.println("Fullname is: " + this.firstName + " " + this.lastName);
	};

	private void printAllCar() {
		for (int i = 0; i < this.cars.size(); i++) {
			this.cars.get(i).print();
		}
	}

	public void printPerson() {
		System.out.println("Th�ng tin person: ");
		System.out.println("Fullname is: " + this.firstName + " " + this.lastName);
		System.out.println("Id is: " + this.id);
		System.out.println("Age is: " + this.age);
		System.out.println("Country is: " + this.country);
		System.out.println("Danh s�ch xe person");
		this.printAllCar();

	};

}
